import 'package:flutter/material.dart';
const navigationButtonColor = Color(0xFF8a2245);

class MatchPrentButton extends StatelessWidget {
  final String title;
  final VoidCallback onPressed;

  MatchPrentButton({this.onPressed, this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      child: FlatButton(
        color: navigationButtonColor,
        onPressed: onPressed,
        child: Container(
          alignment: Alignment.center,
          child: Text(title, style: TextStyle(color: Colors.white),/*style: getMatchParentButtonStyle()*/),
        ),
      ),
    );
  }
}
